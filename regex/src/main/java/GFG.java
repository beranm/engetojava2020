import java.util.regex.*;

public class GFG {
    public static void main(String[] args)
    {

        // Get the regex to be checked 
        String regex = "^CheckHeader=(.+)$";

        // Create a pattern from regex 
        Pattern pattern
                = Pattern.compile(regex);

        // Get the String to be matched 
        String stringToBeMatched
                = "CheckHeader=src/test/resources/header.txt";

        // Create a matcher for the input String 
        Matcher matcher
                = pattern
                .matcher(stringToBeMatched);

        // Get the current matcher state 
        MatchResult result
                = matcher.toMatchResult();
        System.out.println("Current Matcher: "
                           + result);

        while (matcher.find()) {
            // Get the group matched using group() method
            System.out.println(matcher.group(1));
        }
    }
} 