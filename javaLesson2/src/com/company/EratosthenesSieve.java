package com.company;

public class EratosthenesSieve {
    private Boolean[] sieve;
    private Integer size;

    public Boolean[] getSieve() {
        return sieve;
    }

    public EratosthenesSieve(Integer size) {
        sieve = new Boolean[size];
        this.size = size;
        for (int i = 0; i < size; i++) {
            this.sieve[i] = true;
        }
    }
    public EratosthenesSieve sieveNumbers(){
        for (int i = 2; i < Math.sqrt(this.size); i++) {
            if (this.sieve[i]) {
                for (int j = i + i; j < this.size; j += i) {
                    this.sieve[j] = false;
                }
            }
        }
        return this;
    }
    public void printSieve(){
        for (int i = 2; i < this.size; i++) {
            String aBroadStatementAboutPrimeNumber = this.sieve[i] ? "prime" : "not prime";
            System.out.println(String.valueOf(i) + " " + aBroadStatementAboutPrimeNumber);
        }
    }
}
