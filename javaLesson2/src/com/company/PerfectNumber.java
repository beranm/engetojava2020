package com.company;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class PerfectNumber {

    public Integer limit;

    public PerfectNumber(Integer limit) {
        this.limit = limit;
    }

    public ArrayList<Integer> findDivs(Integer i){
        ArrayList<Integer> out = new ArrayList<>();
        for (Integer j = 1; j < i/2 + 1; j ++) {
            if (i % j == 0) {
                out.add(j);
            }
        }
        return out;
    }

    public Integer findDirectlyByNumber(Integer i){
        Integer tmp = 0;
        for (int j = 1; j < i/2 + 1; j ++) {
            if (i % j == 0) {
                tmp += j;
            }
        }
        if (tmp.equals(i)) {
            System.out.println("A numero perfecto " + String.valueOf(i));
        }
        return i;
    }

    public Integer findByNumber(Integer i){
        Integer tmp = 0;
        for (Integer j: findDivs(i)) {
            tmp += j;
        }
        if (tmp.equals(i)) {
            System.out.println("A numero perfecto " + String.valueOf(i));
        }
        return i;
    }

    public void findAsync(){
        ArrayList<CompletableFuture<Integer>> futures = new ArrayList<>();
        for (int i = 1; i < this.limit; i++) {
            Integer finalI = i;
            futures.add(CompletableFuture.supplyAsync(() -> this.findDirectlyByNumber(finalI)));
        }
        for (int i = 0; i < this.limit - 1; i++) {
            try {
                futures.get(i).get();  // silly way to wait for all futures to finnish
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
    public void find(){
        ArrayList<CompletableFuture<Integer>> futures = new ArrayList<>();
        for (int i = 1; i < this.limit; i++) {
            this.findByNumber(i);
        }
    }
    public void findImproved(){
        ArrayList<CompletableFuture<Integer>> futures = new ArrayList<>();
        for (int i = 1; i < this.limit; i++) {
            this.findDirectlyByNumber(i);
        }
    }
}
