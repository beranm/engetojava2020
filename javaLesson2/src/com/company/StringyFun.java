package com.company;

import java.util.Scanner;

public class StringyFun {

    Scanner scanner = new Scanner(System.in);

    private boolean isPalyndrom(String input){
        for (int i = 0; i < input.length() / 2; i++) {
            if (input.charAt(i) != input.charAt(input.length() - 1 - i)){
                return false;
            }
        }
        return true;
    }

    public void printReverse(String input){
        System.out.println("Imma reverse symbols in word");
        input = scanner.nextLine();

        for (int i = input.length() - 1; i >= 0; i --) {
            System.out.print(input.charAt(i));
        }
        System.out.println();
    }

    public void printIfPalyndrom(String input){
        System.out.println("Imma say if palyndrom");
        input = scanner.nextLine();
        System.out.println(isPalyndrom(input));
    }

    public void printIfUpperCased(String input){
        System.out.println("Imma say if only has upper cases");
        input = scanner.nextLine();
        System.out.println(input.toUpperCase().equals(input));
    }

    public void printHeadliner(){
        System.out.println("Imma say headliner");
        String aLine = scanner.nextLine();
        for (String word: aLine.split(" ")
        ) {
            if (word.length() > 1)
                System.out.print(word.substring(0, 1).toUpperCase() + word.substring(1));
            else
                System.out.print(word.substring(0, 1).toUpperCase());
            System.out.print(" ");
        }
    }

}
