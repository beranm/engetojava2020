package com.company;

public class Main {

    public static void main(String[] args) {
        PerfectNumber perfectNumber = new PerfectNumber(200000);
        long startTime = System.nanoTime();
        perfectNumber.find();
        long elapsedTime = System.nanoTime() - startTime;
        System.out.println("Non async silly version time: "
                + elapsedTime/1000000);

        startTime = System.nanoTime();
        perfectNumber.findImproved();
        elapsedTime = System.nanoTime() - startTime;
        System.out.println("Non async improved version time: "
                + elapsedTime/1000000);

        startTime = System.nanoTime();
        perfectNumber.findAsync();
        long elapsedAsyncTime = System.nanoTime() - startTime;
        System.out.println("Async version time: "
                + elapsedAsyncTime/1000000);

        //        EratosthenesSieve sieve = new EratosthenesSieve(100);
//        sieve.sieveNumbers().printSieve();
    }
}
