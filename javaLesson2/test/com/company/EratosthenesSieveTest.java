package com.company;

import org.junit.Test;
import static org.junit.Assert.*;

public class EratosthenesSieveTest {

    @Test
    public void fiveIsAPrimeNumber(){
        EratosthenesSieve tester = new EratosthenesSieve(7);
        assertEquals(tester.sieveNumbers().getSieve()[5], true);
    }
}