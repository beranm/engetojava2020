package com.company;

public class Matrix {

    float [][] m;

    public Matrix(float[][] a) {
        this.m = a;
    }

    public Matrix multiply (float[][] b) {
        float[][] tmp = new float[this.m.length][b.length];
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                for (int k = 0; k < this.m[0].length; k++) {
                    tmp[i][j] = tmp[i][j] + this.m[i][k]*b[k][j];
                }
            }
        }
        this.m = tmp;
        return this;
    }

    
    public String toString() {
        String out = "";
        for (float[] floats : this.m) {
            for (float aFloat : floats) {
                out += " " + String.valueOf(aFloat);
            }
            out += "\n";
        }
        return out;
    }
}
