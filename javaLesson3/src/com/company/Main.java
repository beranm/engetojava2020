package com.company;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // autoboxing vs autounboxing

        Object x = 123;
        System.out.println(x.getClass().getName());  // gives Integer

        System.out.println(x.equals(123));

        // sum some array lists
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            a.add(1);
            b.add(1);
        }
        ArrayList<Integer> c = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            c.add(1);
        }
        FieldsOperation sumArray = new FieldsOperation();
        ArrayList<Integer> sum;
        sum = sumArray.sum(a, b, c);
        for (Integer integer : sum) {
            System.out.println(integer);
        }

        // find a word
        ArrayList<String> blaList = new ArrayList<>();
        blaList.add("abc");
        blaList.add("cbd");
        blaList.add("opk");
        WordyOperation wordyOperation = new WordyOperation();
        System.out.println(wordyOperation.find("abc", blaList));

        // sum matrix
        float[][] d = {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        float[][] e = {{3, 2, 1}, {3, 2, 1}, {3, 2, 1}};
        Matrix m = new Matrix(d);

        System.out.println(m.multiply(e));
    }
}