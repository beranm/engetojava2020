package com.company;

import java.util.ArrayList;
import java.util.List;

public class FieldsOperation {

    @SafeVarargs
    public final ArrayList<Integer> sum(List<Integer>... l){
        ArrayList<Integer> sum = new ArrayList<>();
        for (List<Integer> i:l) {
            for (int j = 0; j < i.size(); j++) {
                try {
                    sum.set(j, sum.get(j) + i.get(j));
                } catch ( IndexOutOfBoundsException e ) {
                    sum.add(j, i.get(j));
                }
            }
        }
        return sum;
    }

    public boolean areSame(List<Integer> a, List<Integer> b){
        if (a.size() != b.size())
            return false;
        for (int i = 0; i < a.size(); i++) {
            if (!a.get(i).equals(b.get(i))){
                return false;
            }
        }
        return true;
    }

}
