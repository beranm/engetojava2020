package com.company;

import java.util.List;

public class WordyOperation {
    public boolean find(String what, List<String> where){
        for (String s : where) {
            if (s.equals(what))
                return true;
        }
        return false;
    }
}
