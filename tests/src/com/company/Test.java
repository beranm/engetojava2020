package com.company;

import java.util.HashMap;
import java.util.Map;

interface Operation {
    public Double count(Double a, Double b);
}

class Test {

    public static void main(String[] args) throws Exception {
        Map<Character, Operation> commands = new HashMap<>();

        // Populate commands map
        commands.put('h', new Operation() {
            public Double count(Double a, Double b) { return  a + b; };
        });
        // Invoke some command
        char cmd = 'h';
        System.out.println(commands.get(cmd).count(1.0, 2.0));
    }
}