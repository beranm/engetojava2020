package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    static Deque<String> operators = new ArrayDeque<>();
    static Deque<Double> values = new ArrayDeque<>();
    static Map<Character, Operation> operations = new HashMap<>();

    public static void main(String[]args){
        operations.put('+', new Operation() {
            public Double count(Double a, Double b) { return  a + b; };
        });
        operations.put('-', new Operation() {
            public Double count(Double a, Double b) { return  a - b; };
        });
        operations.put('*', new Operation() {
            public Double count(Double a, Double b) { return  a * b; };
        });
        operations.put('/', new Operation() {
            public Double count(Double a, Double b) { return  a / b; };
        });
        operations.put('^', new Operation() {  // should be XOR but meh, let's make it POWER of
            public Double count(Double a, Double b) { return Math.pow(a, b); };
        });

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in)
        );
        boolean lastReadWasANumber = false;
        boolean lastReadWasADotNumber = false;
        int lastReadWasADotNumberCounter = 1;
        int c = 0;
        while(true) {
            try {
                if ((c = reader.read()) == -1) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            String cstr = "" + ((char) c);
            if (c == ' ') {
                continue;
            } else if (c == '.') {
                lastReadWasADotNumber = true;
            }else if (Character.isDigit(c)){
                Double value = .0;
                if (lastReadWasANumber)
                    value = values.pop();
                if (lastReadWasADotNumber) {
                    Double appendNumber = Math.pow(.1, lastReadWasADotNumberCounter) * Integer.parseInt(String.valueOf(cstr));
                    values.push(value + appendNumber);
                    lastReadWasADotNumberCounter ++;
                }else
                    values.push(10 * value + Integer.parseInt(String.valueOf(cstr)));
                lastReadWasANumber = true;
            }else if(operations.containsKey((char) c)){
                lastReadWasANumber = false;
                lastReadWasADotNumber = false;
                lastReadWasADotNumberCounter = 1;
                operators.push(cstr);
            }
        }
        if (values.isEmpty()){
            System.out.println("0");
            return;
        }
        Double value = values.pop();
        while (!values.isEmpty()){
            Double poppedValue = values.pop();
            String operator = operators.pop();
            System.out.println(operator);
            value = operations.get(operator.charAt(0)).count(value, poppedValue);
        }
        System.out.println(value);
    }
}
