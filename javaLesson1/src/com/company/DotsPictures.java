package com.company;

public class DotsPictures {
    /**
     *
     * @param count
     */
    public static void printStars(Integer count) {
        for (int i = 0; i < count; i++) {
            System.out.println("*".repeat(i));
        }
    }
    public static void printTree(Integer count) {
        for (int i = 0; i < count; i++) {
            System.out.print(" ".repeat(count - i));
            System.out.print("*".repeat((i*2 + 1)));
            System.out.print(" ".repeat(count - i));
            System.out.println();
        }
    }
    public static void printBox(Integer a, Integer b) {
        for (int i = 0; i < b; i++) {
            if (i == 0 || i == b - 1)
                System.out.println("*".repeat(a));
            else {
                System.out.println("*" + " ".repeat(a-2) + "*");
            }
        }
    }
    public static void printTwoTrees(Integer count){
        for (int i = count - 1; i >= 1; i --) {
            System.out.print(" ".repeat(count - i));
            System.out.print("*".repeat((i*2 + 1)));
            System.out.print(" ".repeat(count - i));
            System.out.println();
        }
        for (int i = 0; i < count; i++) {
            System.out.print(" ".repeat(count - i));
            System.out.print("*".repeat((i * 2 + 1)));
            System.out.print(" ".repeat(count - i));
            System.out.println();
        }
    }
}
