package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World");
        Scanner scanner = new Scanner(System.in);
        int count;

        System.out.println("Gib number, imma do stars!");
        count = scanner.nextInt();
        DotsPictures.printStars(count);

        System.out.println("Gib another number, imma do tree!");
        count = scanner.nextInt();
        DotsPictures.printTree(count);

        System.out.println("Gib two number, imma do box af!");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        DotsPictures.printBox(a, b);

        System.out.println("Gib another number, imma do two trees!");
        count = scanner.nextInt();
        DotsPictures.printTwoTrees(count);

        System.out.println("Gib two number, imma do multipication af!");
        int l = scanner.nextInt();
        int n = scanner.nextInt();
        AMathAf.printMultiple(l, n);

        System.out.println("Gib another number, imma see if it's prima numba!");
        int numba = scanner.nextInt();
        System.out.println(AMathAf.isPrime(numba));

    }
}
