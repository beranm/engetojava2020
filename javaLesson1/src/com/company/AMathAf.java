package com.company;

public class AMathAf {
    public static boolean isPrime(Integer n){
        for (int i = 2; i < Math.sqrt(n); i ++) {
            if(n % i == 0)
                return false;
        }
        return true;
    }

    public static void printMultiple(Integer l, Integer n){
        for (int i = 1; i < n - 1 ; i++) {
            System.out.println(String.format("%2s * %2s = %2s", i, n, n * i));
            if (l.equals(n))
                break;
        }

    }
}
