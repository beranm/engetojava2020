import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

class PubIp {
    String ip;
    String country;
    String cc;
}

public class Main {

    public static void main(String[] args) {
        Gson g = new Gson();
        URL url = null;
        try {
            url = new URL("https://api.myip.com/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            con.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        BufferedReader in = null;
        try {
            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String inputLine = "";
        PubIp p = null;
        StringBuffer content = new StringBuffer();
        while (true) {
            try {
                if (!((inputLine = in.readLine()) != null)) break;
                if (inputLine.length() > 0)
                    p = g.fromJson(inputLine, PubIp.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (p != null){
            System.out.println(p.country);
            System.out.println(p.cc);
            System.out.println(p.ip);
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
