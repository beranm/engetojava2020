package com.beranm;

import java.nio.file.Path;

interface Operation {
    public String check(Path checkFile);
}

