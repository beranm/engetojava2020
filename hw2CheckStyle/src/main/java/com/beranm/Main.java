package com.beranm;

import com.beust.jcommander.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;

import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    @Parameter(names = {"--configuration-file", "-c"})
    String configurationPath = "checkstyle.config";
    @Parameter(names = {"--path", "-p"})
    String path = "./";

    public static void main(String... argv) {
        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(argv);
        main.run();
    }

    public List<String> listFiles(String path) {
        List<String> returnList = new ArrayList<String>();
        File checkFile = new File(path);
        Pattern pattern = Pattern.compile("^.*\\.java$");
        Matcher matcher = pattern.matcher(path);
        if (checkFile.isFile() && matcher.find()) {
            returnList.add(path);
        } else if (checkFile.isDirectory() && !path.equals(".") && !path.equals("..")) {
            File[] fileList = checkFile.listFiles();
            if (fileList != null)
                for (File file : fileList) {
                    returnList.addAll(this.listFiles(file.getPath()));
                }
        }
        return returnList;
    }

    public void run() {
        ArrayList<CompletableFuture<String>> futures = new ArrayList<>();
        if (Files.exists(Paths.get(configurationPath))) {
            if (Files.exists(Paths.get(path))) {
                ReadConfig config = new ReadConfig(Paths.get(configurationPath));
                config.parse();
                List<String> files = listFiles(path);
                for (String filePath : files) {
                    for (Map.Entry<String, Operation> entry : config.operations.entrySet()) {
                        Operation operation = entry.getValue();
                        futures.add(CompletableFuture.supplyAsync(() -> operation.check(Paths.get(filePath))));
                    }
                }
                for (CompletableFuture<String> i: futures) {
                    try {
                        System.out.print(i.get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                System.out.println("Failed to read path to check");
            }
        } else {
            System.out.println("Failed to read config file, path is missing");
        }
    }
}