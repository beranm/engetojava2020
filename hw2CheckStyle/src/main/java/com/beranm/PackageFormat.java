package com.beranm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PackageFormat implements Operation {

    private String errorFormat = "%s: wrong package format\n";

    public String check(Path checkFile) {
        String returnVal = "";
        Pattern pattern = Pattern.compile("^package *[a-z]+(\\.[a-zA-Z_][a-zA-Z0-9_]*)*;$");
        try(BufferedReader br = new BufferedReader(new FileReader(checkFile.toString()))) {
            for(String line; (line = br.readLine()) != null; ) {
                if (line.startsWith("package")){
                    Matcher matcher = pattern.matcher(line);
                    if (! matcher.matches()){
                        returnVal += String.format(
                            errorFormat,
                            checkFile.toString()
                        );
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnVal;
    }
}