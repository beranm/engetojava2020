package com.beranm;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadConfig {
    Path configFile;
    public Map<String, Operation> operations = new HashMap<>();

    public ReadConfig(Path configFile) {
        this.configFile = configFile;
    }
    public void parse(){
        Pattern lineLengthPattern = Pattern.compile("^LineLength=([0-9]+)$");
        Pattern lineCheckHeader = Pattern.compile("^CheckHeader=(.+)$");
        try(BufferedReader br = new BufferedReader(new FileReader(this.configFile.toString()))) {
            for(String line; (line = br.readLine()) != null; ){
                if (operations.containsKey(line)){
                    System.out.println(line + " option already set");
                }
                Matcher lineLengthMatcher = lineLengthPattern.matcher(line);
                Matcher lineCheckHeaderMatcher = lineCheckHeader.matcher(line);
                if (line.equals("TabChar") || line.equals("NewlineAtEnd") || line.equals("PackageFormat")){
                    operations.put(line, new TabChar());
                }else if (lineLengthMatcher.find()){
                    String lineLengthValue = "120";
                    try {
                        lineLengthValue = lineLengthMatcher.group(1);
                    } catch (IndexOutOfBoundsException e){
                        System.out.println("Wrong config line: " + line);
                    }
                    operations.put("LineLength", new LineLength(Integer.parseInt(lineLengthValue)));
                }else if (lineCheckHeaderMatcher.find()){
                    String checkHeaderValue = "header.txt";
                    try {
                        checkHeaderValue = lineCheckHeaderMatcher.group(1);
                    } catch (IndexOutOfBoundsException e){
                        System.out.println("Wrong config line: " + line);
                    }
                    File checkFile = new File(checkHeaderValue);
                    if (! checkFile.exists()){
                        throw new FileNotFoundException("Given " + checkHeaderValue + " file was not located");
                    }
                    operations.put("CheckHeader", new CheckHeader(Paths.get(checkHeaderValue)));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
