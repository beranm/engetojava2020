package com.beranm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class CheckHeader implements Operation {
    private Path headerFile;
    private String errorFormat = "%s: Wrong header\n";

    public CheckHeader(Path headerFile) {
        this.headerFile = headerFile;
    }

    public String check(Path checkFile) {
        try (BufferedReader brHeaderFile = new BufferedReader(new FileReader(this.headerFile.toString()))) {
            try (BufferedReader brCheckFile = new BufferedReader(new FileReader(checkFile.toString()))) {
                for(String headerLine; (headerLine = brHeaderFile.readLine()) != null; ) {
                    String fileLine = brCheckFile.readLine();
                    if (fileLine == null){
                        return "";
                    }
                    if (!fileLine.equals(headerLine)){
                        return String.format(this.errorFormat, checkFile.toString());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}