package com.beranm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class TabChar implements Operation {

    private String errorFormat = "%s: contains tab char at %d:%d\n";

    public String check(Path checkFile){
        String returnVal = "";
        try(BufferedReader br = new BufferedReader(new FileReader(checkFile.toString()))) {
            int lineNum = 0;
            for(String line; (line = br.readLine()) != null; ) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == '\t'){
                        returnVal += String.format(
                            errorFormat,
                            checkFile.toString(),
                            lineNum, i
                        );
                    }
                }
                lineNum ++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnVal;
    }
}
