package com.beranm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class NewlineAtEnd implements Operation {

    private String errorFormat = "%s: does not contain newline at the end of file\n";

    public String check(Path checkFile){
        String returnVal = "";
        try(BufferedReader br = new BufferedReader(new FileReader(checkFile.toString()))) {
            String lastLine = null;
            for(String line; (line = br.readLine()) != null; )
                lastLine = line;
            if (lastLine == null || lastLine.length() > 0){
                returnVal += String.format(
                    errorFormat,
                    checkFile.toString()
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnVal;
    }
}
