package com.beranm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class LineLength implements Operation {

    private int lineLength;
    private String errorFormat = "%s: %d LineLength exceeded: actual length %d, maximum %d\n";

    public LineLength(int lineLength) {
        this.lineLength = lineLength;
    }

    public String check(Path checkFile){
        String returnVal = "";
        try(BufferedReader br = new BufferedReader(new FileReader(checkFile.toString()))) {
            int lineNum = 1;
            for(String line; (line = br.readLine()) != null; ) {
                if (line.length() > this.lineLength){
                    returnVal += String.format(
                        errorFormat,
                        checkFile.toString(),
                        lineNum,
                        line.length(),
                        lineLength
                    );
                }
                lineNum ++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnVal;
    }
}
