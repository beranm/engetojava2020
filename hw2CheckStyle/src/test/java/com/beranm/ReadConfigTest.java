package com.beranm;

import org.junit.Test;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class ReadConfigTest {

    @Test
    public void parse() {
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        String path = LineLengthTest.class.getClassLoader().getResource("checkstyle.config").getPath();
        ReadConfig config = new ReadConfig(Paths.get(path));
        config.parse();
        Map<String, String> operations = new HashMap<String, String>(){
            {
                put("CheckHeader", "src/test/resources/header.txt");
                put("LineLength", "121");
                put("TabChar", "");
                put("NewlineAtEnd", "");
                put("PackageFormat", "");
            }
        };
        assertEquals(operations, config.operations);
    }
}