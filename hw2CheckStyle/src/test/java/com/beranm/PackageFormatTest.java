package com.beranm;

import org.junit.Test;
import java.nio.file.Paths;
import static org.junit.Assert.*;

public class PackageFormatTest {

    @Test
    public void checkWrongPackageFile() {
        String path = LineLengthTest.class.getClassLoader().getResource("fileWithWrongPackage.txt").getPath();
        PackageFormat tstObj = new PackageFormat();
        assertEquals("/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithWrongPackage.txt: wrong package format\n",
            tstObj.check(Paths.get(path)));
    }

    @Test
    public void checkCorrectPackageFile() {
        String path = LineLengthTest.class.getClassLoader().getResource("fileWithPackage.txt").getPath();
        PackageFormat tstObj = new PackageFormat();
        assertEquals("",
                tstObj.check(Paths.get(path)));
    }
}