package com.beranm;

import org.junit.Test;
import java.nio.file.Paths;
import static org.junit.Assert.assertEquals;

public class NewlineAtEndTest {

    @Test
    public void checkEmptyFile() {
        String path = LineLengthTest.class.getClassLoader().getResource("fileWithNothing.txt").getPath();
        NewlineAtEnd tstObj = new NewlineAtEnd();
        assertEquals("/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithNothing.txt: does not contain newline at the end of file\n",
                tstObj.check(Paths.get(path)));
    }

    @Test
    public void checkNonEmptyFile() {
        String path = LineLengthTest.class.getClassLoader().getResource("fileWithNewline.txt").getPath();
        NewlineAtEnd tstObj = new NewlineAtEnd();
        assertEquals("",
                tstObj.check(Paths.get(path)));
    }
}