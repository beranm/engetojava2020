package com.beranm;

import org.junit.Test;
import java.nio.file.Paths;
import static org.junit.Assert.*;

public class LineLengthTest {
    @Test
    public void check() {
        String path = LineLengthTest.class.getClassLoader().getResource("fileWithWrongLineSize.txt").getPath();
        LineLength tstObj = new LineLength(120);
        assertEquals("/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithWrongLineSize.txt: 1 LineLength exceeded: actual length 174, maximum 120\n" +
                     "/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithWrongLineSize.txt: 2 LineLength exceeded: actual length 175, maximum 120\n" +
                     "/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithWrongLineSize.txt: 3 LineLength exceeded: actual length 176, maximum 120\n" +
                     "/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithWrongLineSize.txt: 4 LineLength exceeded: actual length 177, maximum 120\n" +
                     "/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithWrongLineSize.txt: 5 LineLength exceeded: actual length 178, maximum 120\n",
                tstObj.check(Paths.get(path)));
    }
}