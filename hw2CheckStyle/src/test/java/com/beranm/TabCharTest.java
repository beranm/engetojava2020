package com.beranm;

import org.junit.Test;
import java.nio.file.Paths;
import static org.junit.Assert.*;

public class TabCharTest {
    @Test
    public void check() {
        String path = LineLengthTest.class.getClassLoader().getResource("fileWithTabs.txt").getPath();
        TabChar tstObj = new TabChar();
        assertEquals("/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithTabs.txt: contains tab char at 1:6\n" +
                     "/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithTabs.txt: contains tab char at 2:8\n",
                tstObj.check(Paths.get(path)));
    }
}