package com.beranm;

import org.junit.Test;
import java.nio.file.Paths;
import static org.junit.Assert.*;

public class CheckHeaderTest {
    @Test
    public void checkCorrectHeader() {
        String path = CheckHeaderTest.class.getClassLoader().getResource("fileWithCorrectHeader.txt").getPath();
        String headerPath = CheckHeaderTest.class.getClassLoader().getResource("header.txt").getPath();
        CheckHeader tstObj = new CheckHeader(Paths.get(headerPath));
        assertEquals("",
                tstObj.check(Paths.get(path)));
    }

    @Test
    public void checkInCorrectHeader() {
        String path = CheckHeaderTest.class.getClassLoader().getResource("fileWithIncorrectHeader.txt").getPath();
        String headerPath = CheckHeaderTest.class.getClassLoader().getResource("header.txt").getPath();
        CheckHeader tstObj = new CheckHeader(Paths.get(headerPath));
        assertEquals("/home/nemo/javaEngeto2020/hw2CheckStyle/build/resources/test/fileWithIncorrectHeader.txt: Wrong header\n",
                tstObj.check(Paths.get(path)));
    }
}