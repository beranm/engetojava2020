package com.company;

import java.time.LocalDate;
import java.util.ArrayList;

public class Gallery {
    public static class Room {
        int id;
        String type;
    }
    public abstract static class Employee {}
    public static class Guard extends Employee {
        ArrayList<Schedule> schedules;
    }
    public static class KnowledgeMan extends Employee {}
    public static class Schedule {
        enum SEASON {

        }
    }
    public static class Picture {
        String name;
        String author;
        LocalDate creationDate;
        ArrayList<LocalDate> datesOfStealAttempts;
    }

}
