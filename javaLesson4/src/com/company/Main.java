package com.company;

class Zoo {
    public static void main(String[] args) {
        Animal[] zoo = new Animal[] {
                new Zebra(10, "Alojz"),
                new Tiger(4, "Ezechiel")
        };
        for (int i = 0; i < zoo.length; i++) {
            zoo[i].sayHi();
            zoo[i].eat(new Grass(50));
            zoo[i].eat(new Meat(100));
        }
    }
    private abstract static class Animal {
        private int age;
        private String name;
        public Animal(int age, String name) {
            this.age = age;
            this.name = name;
        }
        public abstract void sayHi();
        public abstract void eat(Food f);
    }
    private static class Tiger extends Animal {
        public Tiger(int age, String name) {
            super(age, name);
        }
        @Override
        public void sayHi() {
            System.out.println("Hi, I'm tiger");
        }
        @Override
        public void eat(Food f) {
            if (f instanceof Meat) {
                System.out.println("chew chew");
            } else {
                System.out.println("I don't eat this");
            }
        }
    }
    private static class Zebra extends Animal implements Herbivore {
        public Zebra(int age, String name) {
            super(age, name);
        }
        @Override
        public void sayHi() {
            System.out.println("Hi, I'm zebra");
        }
        @Override
        public void eat(Food f) {
            System.out.println("I eat everything");
        }
        @Override
        public void eat(Grass g) {
            System.out.println("I love grass");
        }
    }
    private abstract static class Food {
        public int calories;
        public Food(int calories) {
            this.calories = calories;
        }
    }
    private static class Meat extends Food {
        public Meat(int calories) {
            super(calories);
        }
    }
    private static class Grass extends Food {
        public Grass(int calories) {
            super(calories);
        }
    }
    private interface Herbivore {
        void eat(Grass g);
    }
}
