import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class ParallelMergeSortTest {

    @Test
    public void compute() {
        List<Integer> toSortParallel = new ArrayList<Integer>(){
            {
                add(1);
                add(3);
                add(4);
                add(11);
                add(7);
                add(9);
            }
        };
        List<Integer> correctList = new ArrayList<>(toSortParallel);
        ParallelMergeSort parallelMergeSort = new ParallelMergeSort(toSortParallel);
        parallelMergeSort.compute();
        Collections.sort(correctList);
        assertEquals(toSortParallel, correctList);
    }

    @Test
    public void computeLargeSet() {
        int listSize = 10000000;
        List<Integer> toSortParallel = new ArrayList<Integer>();
        List<Integer> correctList = new ArrayList<Integer>();
        for (int i = 0; i < listSize; i ++) {
            toSortParallel.add(listSize - i);
            correctList.add(listSize - i);
        }
        ParallelMergeSort parallelMergeSort = new ParallelMergeSort(toSortParallel);
        parallelMergeSort.compute();
        MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSort(correctList);
        assertEquals(correctList, toSortParallel);
    }
}
