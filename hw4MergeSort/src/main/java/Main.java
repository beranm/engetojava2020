import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int listSize = 10000000;
        List<Integer> toSortParallel = new ArrayList<Integer>();
        List<Integer> correctList = new ArrayList<Integer>();
        for (int i = 0; i < listSize; i ++) {
            toSortParallel.add(listSize - i);
            correctList.add(listSize - i);
        }
        ParallelMergeSort parallelMergeSort = new ParallelMergeSort(toSortParallel);
        parallelMergeSort.compute();
        MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSort(correctList);
    }
}
