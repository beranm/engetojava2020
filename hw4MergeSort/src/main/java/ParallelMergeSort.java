import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

// implemented from https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html
public class ParallelMergeSort<T extends Comparable> extends RecursiveAction {
    static final int THRESHOLD = 1 << 13;
    final List<T> array;
    final int leftIndex, rightIndex;

    ParallelMergeSort(List<T> array, int leftIndex, int rightIndex) {
        this.array = array;
        this.leftIndex = leftIndex;
        this.rightIndex = rightIndex;
    }

    ParallelMergeSort(List<T> array) { this(array, 0, array.size()); }

    @Override
    protected void compute() {
        if (rightIndex - leftIndex < THRESHOLD){
            MergeSort mergeSort = new MergeSort();
            mergeSort.mergeSort(array.subList(leftIndex, rightIndex));
        }else {
            int middleOfTheArray = (rightIndex + leftIndex) / 2;
            invokeAll(
                    new ParallelMergeSort(array, leftIndex, middleOfTheArray),
                    new ParallelMergeSort(array, middleOfTheArray, rightIndex)
            );
            merge(leftIndex, middleOfTheArray, rightIndex);
        }
    }
    void merge(int leftIndex, int middleOfTheArray, int rightIndex) {
        List<T> buf = new ArrayList<T>();
        for (T i: array.subList(leftIndex, middleOfTheArray)) {
            buf.add(i);
        }
        for (int i = 0, j = leftIndex, k = middleOfTheArray; i < buf.size(); j++) {
            if (k == rightIndex || buf.get(i).compareTo(array.get(k)) < 0){
                array.set(j, buf.get(i++));
            }else{
                array.set(j, array.get(k++));
            }
        }
    }
}
