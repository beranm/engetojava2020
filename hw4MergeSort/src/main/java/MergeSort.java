import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MergeSort {
    public static int getRandomDoubleBetweenRange(int min, int max){
        return (int) (Math.random()*((max-min)+1))+min;
    }

    // stole from https://www.algoritmy.net/article/13/Merge-sort just add generics
    private <T extends Comparable> void recursiveMergeSort(List<T> array, List<T> aux, int left, int right) {
        if(left == right || left < 0 || right < 0) return;
        int middleIndex = (left + right)/2;
        recursiveMergeSort(array, aux, left, middleIndex);
        recursiveMergeSort(array, aux, middleIndex + 1, right);
        merge(array, aux, left, right);
        for (int i = left; i <= right; i++) {
            array.set(i, aux.get(i));
        }
    }

    public <T extends Comparable> void recursiveMergeSort(List<T> array, int left, int right) {
        List<T> aux = new ArrayList<T>(Collections.nCopies(array.size(), null));
        recursiveMergeSort(array, aux, left, right);
    }

    private <T extends Comparable> void merge(List<T> array, List<T> aux, int left, int right) {
        int middleIndex = (left + right)/2;
        int leftIndex = left;
        int rightIndex = middleIndex + 1;
        int auxIndex = left;
        while(leftIndex <= middleIndex && rightIndex <= right) {
            if (array.get(leftIndex).compareTo(array.get(rightIndex)) < 0) {
                aux.set(auxIndex, array.get(leftIndex++));
            } else {
                aux.set(auxIndex, array.get(rightIndex++));
            }
            auxIndex ++;
        }
        while (leftIndex <= middleIndex) {
            aux.set(auxIndex, array.get(leftIndex++));
            auxIndex++;
        }
        while (rightIndex <= right) {
            aux.set(auxIndex, array.get(rightIndex++));
            auxIndex++;
        }
    }

    public <T extends Comparable> void mergeSort(List<T> toSort) {
        List<T> aux = new ArrayList<T>(Collections.nCopies(toSort.size(), null));
        recursiveMergeSort(toSort, aux, 0, toSort.size() - 1);
    }
}
