package com.beranm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class HashFiles {

    /*
    source from https://stackoverflow.com/questions/31732207/how-to-calculate-sha512-of-a-file
     */
    public static String hashFile(File file)
            throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        FileInputStream fis = new FileInputStream(file);
        byte[] dataBytes = new byte[1024];

        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
            md.update(dataBytes, 0, nread);
        }

        byte[] mdbytes = md.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static String hashFileSilently(File file){
        try {
            return HashFiles.hashFile(file);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "Hash could not be estimated";
        } catch (IOException e) {
            e.printStackTrace();
            return "Hash could not be estimated";
        }
    }

    public static Map<Path, String> hashAllFiles(Set<Path> files){
        Map<Path, CompletableFuture<String>> returnAsyncMap = new HashMap<>();
        Map<Path, String> returnMap = new HashMap<>();
        for (Path p:
             files) {
            returnAsyncMap.put(p, CompletableFuture.supplyAsync(() -> HashFiles.hashFileSilently(p.toFile())));
        }
        for (Path p:
             files) {
            try {
                returnMap.put(p, returnAsyncMap.get(p).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        return returnMap;
    }
}
