package com.beranm;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class InterpretTheHashFiles {
    public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
        Set<T> keys = new HashSet<T>();
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }

    public static boolean validateDecision(int decision, int scale){
        return decision < scale;
    }

    public static void interpret(Map<Path, String> filesWithHashes){
        Set<String> hashSet = new HashSet<String>();
        Map<Integer, Path> enumeratedPaths = new HashMap<>();
        for (String hash:
                filesWithHashes.values()) {
            hashSet.add(hash);
        }
        for (String hash:
                hashSet) {
            Set<Path> pathsPerHashes = InterpretTheHashFiles.getKeysByValue(filesWithHashes, hash);
            if (pathsPerHashes.size() > 1){
                Scanner userInput = new Scanner( System.in );
                System.out.println("Found duplicate files:");
                int i = 0;
                for (Path p: pathsPerHashes) {
                    enumeratedPaths.put(i, p);
                    System.out.print(i);
                    System.out.println(": " + p.toString());
                    i ++;
                }
                System.out.println("What do you want to do? (0 = ignore, 1 = delete, 2 = create soft link, 3 = create hard link)");
                int a = userInput.nextInt();
                while (! InterpretTheHashFiles.validateDecision(a, 4)){
                    System.out.println("Pick from  0 to 3");
                    a = userInput.nextInt();
                }
                int j = 0;
                switch(a) {
                    default:
                        break;
                    case 1:
                        for (Path p: pathsPerHashes) {
                            System.out.println(String.format("Do you want to delete %s? (y/n)", p.toString()));
                            if (userInput.next().charAt(0) == 'y'){
                                try {
                                    Files.delete(p);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        break;
                    case 2:
                        System.out.println("File for which to create soft link: (file number)");
                        a = userInput.nextInt();
                        while (! InterpretTheHashFiles.validateDecision(a, pathsPerHashes.size())){
                            System.out.print("Pick from  0 to ");
                            System.out.println(String.valueOf(pathsPerHashes.size() - 1));
                            a = userInput.nextInt();
                        }
                        j = 0;
                        for (Path p: pathsPerHashes) {
                            if (j != a){
                                try {
                                    Files.delete(p);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Files.createSymbolicLink(p, enumeratedPaths.get(a));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            j ++;
                        }
                        break;
                    case 3:
                        System.out.println("File for which to create hard link: (file number)");
                        a = userInput.nextInt();
                        while (! InterpretTheHashFiles.validateDecision(a, pathsPerHashes.size())){
                            System.out.print("Pick from  0 to ");
                            System.out.println(String.valueOf(pathsPerHashes.size() - 1));
                            a = userInput.nextInt();
                        }
                        j = 0;
                        for (Path p: pathsPerHashes) {
                            if (j != a){
                                try {
                                    Files.delete(p);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Files.createLink(p, enumeratedPaths.get(a));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            j ++;
                        }
                        break;
                }
            }
        }
    }
}
