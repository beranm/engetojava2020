package com.beranm;

import java.io.*;
import java.net.Socket;
import java.text.ParseException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Client extends Thread {
    private Socket socket;
    public String path = null;

    public Client(Socket socket) {
        this.socket = socket;
    }

    private void returnStringToSocket(String line, OutputStream os) throws IOException {
        for (int c: line.getBytes()) {
            os.write((byte)c);
            if (c == '\n') {
                os.flush();
            }
        }
   }

    public void run() {
        try {
            InputStream input = this.socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line = null;
            while ((line = reader.readLine()) != null)
                if (line.length() > 0) {
                    try {
                        this.parseHeader(line);
                    } catch (ParseException e) {
                        System.out.println("Unknown header: " + line);
                    }
                }
                else
                    break;
            System.out.println("Input done");
            OutputStream os = socket.getOutputStream();
            File f = new File(this.path);
            if (this.path == null){
                this.returnStringToSocket("HTTP/1.0 400 Bad Request\n\n", os);
            }else if (f.exists() && !f.isDirectory()){
                File file = new File(this.path);
                Scanner scanner = new Scanner(file);
                this.returnStringToSocket("HTTP/1.0 200 OK\n\n", os);
                while (scanner.hasNextLine()) {
                    String data = scanner.nextLine();
                    this.returnStringToSocket(data + "\n", os);
                    System.out.println(data);
                }
                scanner.close();
            }else{
                this.returnStringToSocket("HTTP/1.0 400 Bad Request\n\n", os);
            }
            os.close();
            this.socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void parseHeader(String line) throws ParseException {
        Pattern httpGet = Pattern.compile("^GET (.*) HTTP/1.[0,1]$");
        Matcher matcher = httpGet.matcher(line);
        matcher.find();
        try{
            this.path = "." + matcher.group(1);
        } catch (IndexOutOfBoundsException | IllegalStateException e){
            throw new ParseException("Wrong HTTP header for the content", 0);
        }
    }
}
