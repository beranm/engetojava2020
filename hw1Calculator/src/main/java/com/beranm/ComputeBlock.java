package com.beranm;

import java.awt.*;
import java.util.*;
import static java.util.Map.entry;

public class ComputeBlock {
    private Deque<String> operators = new ArrayDeque<>();
    private Deque<Double> values = new ArrayDeque<>();
    private static Map<Character, Operation> operations = new HashMap<Character, Operation>() {
        {
            put('+', Double::sum);
            put('-', (a, b) -> a - b);
            put('*', (a, b) -> a * b);
            put('/', (a, b) -> a / b);
            put('^', Math::pow);
        }
    };
    private static Map<String, String> operatorDoubles = new HashMap<String, String>() {
        {
            put("++", "+");
            put("--", "+");
            put("+-", "-");
            put("-+", "-");
        }
    };
    private static Set<Character> preferredOperation = new HashSet <>(Arrays.asList('*', '/', '^'));
    private boolean lastReadWasANumber = false;
    private boolean lastReadWasAnOperator = false;
    private boolean lastReadWasADotNumber = false;
    private int lastReadWasADotNumberCounter = 1;
    private int i = 0;

    public double parseAndReturnValue(String inputLine){
        for ( ; this.i < inputLine.length(); this.i ++){
            int c = inputLine.charAt(this.i);
            String cstr = "" + ((char) c);
            if (c == ' ') {
                continue;
            } else if (c == '(') {
                ComputeBlock computeBlock = new ComputeBlock();
                computeBlock.i = this.i + 1;
                values.push(computeBlock.parseAndReturnValue(inputLine));
                this.i = computeBlock.i;
                if (this.i == inputLine.length()){
                    throw new StringIndexOutOfBoundsException("Missing closing bracket");
                }
                if (!operators.isEmpty() && operators.peekFirst().charAt(0) == '-'){
                    operators.pop();
                    operators.push("+");
                    values.push(-1 * values.pop());
                }
                lastReadWasANumber = true;
                lastReadWasAnOperator = false;
            } else if (c == ')') {
                break;
            } else if (c == '.') {
                if (!lastReadWasANumber)
                    throw new IllegalArgumentException("Double number defined without leading number character");
                lastReadWasADotNumber = true;
            }else if (Character.isDigit(c)){
                Double value = .0;
                if (lastReadWasANumber)
                    value = values.pop();
                if (lastReadWasADotNumber) {
                    Double appendNumber = Math.pow(.1, lastReadWasADotNumberCounter) * Integer.parseInt(String.valueOf(cstr));
                    if (value >= 0)
                        values.push(value + appendNumber);
                    else
                        values.push(value - appendNumber);
                    lastReadWasADotNumberCounter ++;
                }else
                if (value >= 0)
                    values.push(10 * value + Integer.parseInt(String.valueOf(cstr)));
                else
                    values.push(10 * value - Integer.parseInt(String.valueOf(cstr)));
                if (!operators.isEmpty() && operators.peekFirst().charAt(0) == '-'){
                    operators.pop();
                    operators.push("+");
                    values.push(-1 * values.pop());
                }
                lastReadWasANumber = true;
                lastReadWasAnOperator = false;
            }else if(operations.containsKey((char) c)){
                if (!operators.isEmpty() && preferredOperation.contains(operators.peekFirst().charAt(0))){
                    if (!values.isEmpty()){
                        Double value = values.pop();
                        Double poppedValue = values.pop();
                        String operator = operators.pop();
                        values.push(operations.get(operator.charAt(0)).count(poppedValue, value));
                    }
                }
                if (!operators.isEmpty() && lastReadWasAnOperator && !lastReadWasANumber && !preferredOperation.contains((char) c)){
                    String operator = operators.pop();
                    operators.push(operatorDoubles.get(operator + cstr));
                }else
                    operators.push(cstr);
                lastReadWasANumber = false;
                lastReadWasAnOperator = true;
                lastReadWasADotNumber = false;
                lastReadWasADotNumberCounter = 1;
            }
        }
        if (values.isEmpty()){
            throw new IllegalStateException("Empty list of values");
        }
        Double value = values.pop();
        while (!values.isEmpty()){
            Double poppedValue = values.pop();
            String operator = operators.pop();
            value = operations.get(operator.charAt(0)).count(poppedValue, value);
        }
        if (!operators.isEmpty() && operators.peekFirst().charAt(0) == '+')
            operators.pop();
        if (!operators.isEmpty())
            throw new IllegalStateException("Too many operators for less values");
        return value;
    }
}
