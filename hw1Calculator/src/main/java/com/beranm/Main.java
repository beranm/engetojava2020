package com.beranm;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[]args){
        System.out.println("Write your calculation expression:");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while ((line = reader.readLine()) != null) {
                ComputeBlock computeBlock = new ComputeBlock();
                System.out.print( ">" );
                System.out.println(computeBlock.parseAndReturnValue(line));
            }
        }
        catch( Exception e ){
            e.printStackTrace();
        }
    }
}
