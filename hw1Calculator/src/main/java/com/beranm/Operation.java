package com.beranm;

interface Operation {
    double count(double a, double b);
}
