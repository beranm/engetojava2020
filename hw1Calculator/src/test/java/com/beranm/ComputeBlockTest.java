package com.beranm;

import static junit.framework.TestCase.assertEquals;

public class ComputeBlockTest {

    @org.junit.Test(expected = IllegalStateException.class)
    public void parseEmpty() {
        ComputeBlock computeBlock = new ComputeBlock();
        computeBlock.parseAndReturnValue("");
    }

    @org.junit.Test(expected = IllegalStateException.class)
    public void parseSimpleNotClosingBracket() {
        ComputeBlock computeBlock = new ComputeBlock();
        computeBlock.parseAndReturnValue("(");
    }

    @org.junit.Test(expected = StringIndexOutOfBoundsException.class)
    public void parseSimpleNotThatSimpleClosingBracket() {
        ComputeBlock computeBlock = new ComputeBlock();
        computeBlock.parseAndReturnValue("((4 + 3)");
    }

    @org.junit.Test(expected = IllegalStateException.class)
    public void parseOnlyOperator() {
        ComputeBlock computeBlock = new ComputeBlock();
        computeBlock.parseAndReturnValue("   +   ");
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void wrongFormattedDouble() {
        ComputeBlock computeBlock = new ComputeBlock();
        computeBlock.parseAndReturnValue(".4");
    }

    @org.junit.Test
    public void parseSimpleExpression() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals(3.0, computeBlock.parseAndReturnValue("1+2"));
    }

    @org.junit.Test
    public void parseSimpleExpressionWithMinus() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((1.0-2.0), computeBlock.parseAndReturnValue("1-2"));
    }

    @org.junit.Test
    public void parseFloatingNumber() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((1.123-2), computeBlock.parseAndReturnValue("1.123-2"));
    }

    @org.junit.Test
    public void parseBrackets() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals(((1.123-2)+10-100), computeBlock.parseAndReturnValue("(1.123-2)+10-100"));
    }

    @org.junit.Test
    public void parseSimpleMultiply() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((2.0*3.0*5.0), computeBlock.parseAndReturnValue("2*3*5"));
    }

    @org.junit.Test
    public void parseHarderMultiply() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((10+2.0*3.0*5.0+10), computeBlock.parseAndReturnValue("10 + 2*3*5 + 10"));
    }

    @org.junit.Test
    public void parseHarderMultiplyWithBrackets() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((10+2.0*3.0*(5.0+6.0)+10), computeBlock.parseAndReturnValue("10 + 2*3*(5+6) + 10"));
    }

    @org.junit.Test
    public void parseHarderDivideWithBrackets() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((10.0+100*25*5+10), computeBlock.parseAndReturnValue("10 + 100*25*(5+0) + 10"));
    }

    @org.junit.Test
    public void parsePowerOfNumber() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((10+10000.0), computeBlock.parseAndReturnValue("10 + 10^2^2"));
    }

    @org.junit.Test
    public void parseNegativeNumber() {
        ComputeBlock computeBlock = new ComputeBlock();
        assertEquals((0.0), computeBlock.parseAndReturnValue("1+-1"));
    }

    @org.junit.Test
    public void parseNestedBrackets(){
        ComputeBlock computeBlock = new ComputeBlock();
        // assertEquals(((-3.2) * (((-4 + 8) + 2) - 3)), computeBlock.parseAndReturnValue("(-3.2) * (((-4 + 8) + 2) - 3)"));
        assertEquals((-3.2) *(((-4 + 8) + 2) - 3), computeBlock.parseAndReturnValue("(-3.2) * (((-4 + 8) + 2) - 3)"));
    }
}