public class Box<T> {
    T valueInTheBox;

    public T getValueInTheBox() {
        return valueInTheBox;
    }

    public void setValueInTheBox(T valueInTheBox) {
        this.valueInTheBox = valueInTheBox;
    }
}
