import java.util.Objects;

public class Pair<K, V> {
    public K x;
    public V y;

    public static <K, V> boolean equals(Pair<K, V> p1, Pair<K, V> p2) {
        return (p1.x.equals(p2.x) && p1.y.equals(p2.y));
    }

    public static <T> Pair<T, T> twice(T param){
        Pair<T, T> r = new Pair<T, T>();
        r.x = param;
        r.y = param;
        return r;
    }

    @Override
    public String toString() {
        return "Pair{" +
               "x=" + x +
               ", y=" + y +
               '}';
    }
}
