import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Box<String> b = new Box<>();
        b.setValueInTheBox("abcd");
        System.out.println(b.getValueInTheBox());

        Pair<Integer, String> a = new Pair<>();
        a.x = 1;
        a.y = "a";

        Pair<Integer, Integer> p = Pair.twice(1);
        System.out.println(p);

        Integer [] i = {0, 1, 2, 3};
        // Lst.<Integer>insertAsFirstElem(i, 6);
        Lst.insertAsFirstElem(i, 6);
        System.out.println(i[0]);

        List<Integer> o = new ArrayList<>(List.of(1, 2, 3));
        System.out.println(Lst.sumOfList(o));

        Lst.printList(new ArrayList<>(List.of(1, 2, 3)));
        System.out.println();
        List<Integer> c = new ArrayList<>();
        Lst.addAll(c, i);
        Lst.printList(c);
    }
}
