import java.util.List;

public class Lst {
    public static <T> void insertAsFirstElem(T[] arr, T elem){
        arr[0] = elem;
    }
    public static <T extends Number> Double sumOfList(List<T> l){
        double accumulator = 0.0;
        for (T i:
             l) {
            accumulator = accumulator + i.doubleValue();
        }
        return accumulator;
    }
    public static void printList(List<?> l){
        for (Object i:
             l) {
            System.out.println(i);
        }
    }
    public static <T> void addAll(List<T> l, T [] arr){
        l.addAll(List.of(arr));
    }
}
