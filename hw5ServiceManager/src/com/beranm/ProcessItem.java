package com.beranm;

import javax.lang.model.type.UnionType;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

enum Health {
    HEALTHY,
    FAILED,
}

public class ProcessItem {
    LinkedList<Health> healthChecks = new LinkedList<>(){
        {
            add(Health.HEALTHY);
            add(Health.HEALTHY);
            add(Health.HEALTHY);
        }
    };

    String serviceName = null;
    String program = "";
    String runArgs = "";
    int port = 80;
    boolean enabled = true;
    String healthEndpoint = "/";

    private Process p = null;

    public void loadProperties(Properties props) {
        Set<String> keys = props.stringPropertyNames();
        for (String key : keys) {
            String value = props.get(key).toString();
            switch (key){
                case "SERVICE_NAME":
                    this.serviceName = value;
                    break;
                case "PROGRAM":
                    this.program = value;
                    break;
                case "RUN_ARGS":
                    this.runArgs = value;
                    break;
                case "PORT":
                    this.port = Integer.parseInt(value);
                    break;
                case "ENABLED":
                    this.enabled = Boolean.parseBoolean(value);
                    break;
                case "HEALTH_ENDPOINT":
                    this.healthEndpoint = value;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + key);
            }
        }
    }

    public void start(){
        try {
            this.p = Runtime.getRuntime().exec(this.program + " " + this.runArgs);
            System.out.print("Starting service " + this.serviceName + ": ");
            System.out.println(this.program + " " + this.runArgs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop(){
        p.destroy();
    }

    public void restart(){
        this.stop();
        this.start();
    }

    public void checkHealth() {
        URL url = null;
        int statusCode = 502;
        try {
            url = new URL("http://localhost:" + this.port + this.healthEndpoint);
        } catch (MalformedURLException e) {
            this.healthChecks.push(Health.FAILED);
        }
        HttpURLConnection http = null;
        try {
            http = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            this.healthChecks.push(Health.FAILED);
            System.out.println("Failed check on http://localhost:" + this.port + this.healthEndpoint);
        }
        try {
            statusCode = http.getResponseCode();
        } catch (IOException e) {
            this.healthChecks.push(Health.FAILED);
            System.out.println("Failed check on http://localhost:" + this.port + this.healthEndpoint);
        }
        if (statusCode == 200)
            this.healthChecks.push(Health.HEALTHY);
        else{
            this.healthChecks.push(Health.FAILED);
        }
        if (this.healthChecks.size() > 3)
            this.healthChecks.removeLast();
    }
}
