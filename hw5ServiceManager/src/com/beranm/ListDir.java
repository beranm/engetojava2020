package com.beranm;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

public class ListDir {
    public static Set<Path> listFiles(Path path) {
        Set<Path> returnList = new HashSet<>();
        if (Files.isRegularFile(path) && !Files.isSymbolicLink(path)) {
            returnList.add(path);
        } else if (Files.isDirectory(path) && !Files.isSymbolicLink(path) && !path.toString().equals(".") && !path.toString().equals("..")) {
            Set<Path> fileList = new HashSet<>();
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
                for (Path p : stream) {
                    returnList.addAll(ListDir.listFiles(p));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            returnList.addAll(fileList);
        }
        return returnList;
    }
}
