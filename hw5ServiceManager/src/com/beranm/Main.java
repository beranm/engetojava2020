package com.beranm;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Input should be `service config dir dir1 dir2 ...`");
            return;
        }
        Set<Path> files = new HashSet<>();
        for (String dir:
                args) {
            files.addAll(ListDir.listFiles(Path.of(dir)));
        }
        ProcessManager processManager = new ProcessManager();
        processManager.parseConfigs(files);
        processManager.startServices();
        processManager.listenToCommands();
    }
}

