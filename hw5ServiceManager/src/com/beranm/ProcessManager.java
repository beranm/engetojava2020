package com.beranm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ProcessManager {

    public static volatile List<ProcessItem> proceses  = new ArrayList<>();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public ProcessManager() {
        scheduler.scheduleAtFixedRate(ProcessManager.checkProcesses(), 15, 15, SECONDS);
    }

    public void startServices() {
        for (ProcessItem p: ProcessManager.proceses) {
            if (p.enabled)
                p.start();
        }
    }

    public static Runnable checkProcesses(){
        return () -> {
            for (ProcessItem p: ProcessManager.proceses) {
                if (p.enabled)
                    p.checkHealth();
                if (p.enabled && p.healthChecks.get(0) == Health.FAILED && p.healthChecks.get(1) == Health.FAILED && p.healthChecks.get(2) == Health.FAILED){
                    p.restart();
                    System.out.println("Restarting " + p.serviceName + " due to failed health check");
                }
            }
        };
    }

    public void parseConfigs(Set<Path> files) {
        for (Path p: files) {
            System.out.print("Loading ");
            System.out.println(p.toString());
            Properties prop = new Properties();
            FileInputStream is = null;
            try {
                is = new FileInputStream(p.toString());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                continue;
            } finally {
                try {
                    prop.load(is);
                } catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            ProcessItem process = new ProcessItem();
            try {
                process.loadProperties(prop);
            } catch (IllegalStateException e){
                e.printStackTrace();
                continue;
            }
            ProcessManager.proceses.add(process);
        }
    }

    public void stopAll() {
        for (ProcessItem p:
                proceses) {
            p.stop();
        }
        scheduler.shutdown();
    }

    public void listenToCommands() {
        Scanner commandScanner = new Scanner(System.in);
        System.out.println("Known commands: show-status, disable serviceName, enable serviceName");
        System.out.println("Enter the command:");
        while (true){
            String command = commandScanner.nextLine();
            Pattern patternDisableProcessName = Pattern.compile("^disable (.*)$");
            Pattern patternEnableProcessName = Pattern.compile("^enable (.*)$");
            if (command.matches("show-status")) {
                for (ProcessItem p: proceses) {
                    System.out.print(p.serviceName + ": ");
                    if (!p.enabled)
                        System.out.println("DISABLED");
                    else if (p.healthChecks.get(0) == Health.FAILED && p.healthChecks.get(1) == Health.FAILED && p.healthChecks.get(2) == Health.FAILED)
                        System.out.println("DOWN");
                    else if (p.healthChecks.get(0) == Health.HEALTHY && p.healthChecks.get(1) == Health.HEALTHY && p.healthChecks.get(2) == Health.HEALTHY)
                        System.out.println("HEALTHY");
                    else
                        System.out.println("TENTATIVE");
                }
            }else if (command.matches("disable .*")) {
                String processName = "";
                Matcher checkProcessName = patternDisableProcessName.matcher(command);
                checkProcessName.find();
                try {
                    processName = checkProcessName.group(1);
                } catch (IndexOutOfBoundsException | IllegalStateException e) {
                    System.out.println("Format should be `disable processName`");
                } finally {
                    for (ProcessItem p: proceses) {
                        if (p.serviceName.equals(processName)){
                            p.enabled = false;
                            System.out.println("Setting " + processName + " to disable");
                        }
                    }
                }
            }else if (command.matches("enable .*")){
                String processName = "";
                Matcher checkProcessName = patternEnableProcessName.matcher(command);
                checkProcessName.find();
                try{
                    processName = checkProcessName.group(1);
                } catch (IndexOutOfBoundsException e){
                    System.out.println("Format should be `disable processName`");
                } finally {
                    for (ProcessItem p: proceses) {
                        if (p.serviceName.equals(processName)){
                            p.enabled = true;
                            System.out.println("Setting " + processName + " to enable");
                        }
                    }
                }
            }
        }
    }
}
