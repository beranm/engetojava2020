# engetoJava2020

I got a voucher on anything from https://engeto.cz/skoleni/ cause once at their pretty expensive training on hacking the WiFi sucked. So I picked 25% off the Java lessons.
The course so far seems to me more about algorithmization rather than Java. The guy, Adam Hornacek, teaching those lessons is ok. So far, he helped me with everything I asked.

If you want to start somewhere and need somebody to shorten googling things around significantly, I would strongly suggest the lectures.

Anyway:

 - some of the examples from the lectures were not implemented here, and they will be not (cause I am interested in Java, not algorithmization)
 - some of them are hopefully implemented in a rather way more exciting way
 - some of them are silly and should be regarded as such by anyone who is Java senior
 - any open issue will be looked upon with favor, and I would be glad for those

