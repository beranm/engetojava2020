package com.beranm;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class Tst {
    private void test (){
        System.out.println("a very private method");
    }
}

public class Main {

    public static void main(String[] args) {
        Integer ob = 1;
        Class c = ob.getClass();
        for (Method method : c.getDeclaredMethods()) {
            System.out.println(method.getName());
        }

        Tst tst = new Tst();
        Method method = null;
        try {
            method = tst.getClass().getDeclaredMethod("test");
            method.setAccessible(true);
            try {
                Object r = method.invoke(tst);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
