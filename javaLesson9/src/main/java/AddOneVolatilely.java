class testyThredyWithVolatilely implements Runnable {
    public static volatile Integer l = 0;

    public static Object lock = new Object();

    private Thread t;
    private String threadName;

    public testyThredyWithVolatilely(String threadName) {
        this.threadName = threadName;
    }

    public void run() {
        int i = 0;
        while (i < 100000000){
            synchronized (lock){
                testyThredyWithVolatilely.l ++;
            }
            i ++;
        }
        System.out.print(this.threadName);
        System.out.println(" done!");
    }

    public void start () {
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }

    public void join(){
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class AddOneVolatilely {
    public static void main(String[] args) {
        testyThredyWithVolatilely R1 = new testyThredyWithVolatilely("first");
        R1.start();
        testyThredyWithVolatilely R2 = new testyThredyWithVolatilely("second");
        R2.start();

        R1.join();
        R2.join();

        System.out.println(testyThredyWithVolatilely.l);
    }
}
