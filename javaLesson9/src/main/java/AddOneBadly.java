class testyThredyWithNoSleepy implements Runnable {
    public static long l = 0;

    private Thread t;
    private String threadName;

    public testyThredyWithNoSleepy(String threadName) {
        this.threadName = threadName;
    }

    public void run() {
        int i = 0;
        while (i < 100000000){
            testyThredyWithNoSleepy.l += 1;
            i ++;
        }
        System.out.print(this.threadName);
        System.out.println(" done!");
    }

    public void start () {
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }

    public void join(){
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class AddOneBadly {
    public static void main(String[] args) {
        testyThredyWithNoSleepy R1 = new testyThredyWithNoSleepy("first");
        R1.start();
        testyThredyWithNoSleepy R2 = new testyThredyWithNoSleepy("second");
        R2.start();

        R1.join();
        R2.join();

        System.out.println(testyThredyWithNoSleepy.l);
    }
}
