class testyThredy implements Runnable {
    static long l = 0;

    private Thread t;
    private String threadName;

    public testyThredy(String threadName) {
        this.threadName = threadName;
    }

    public void run() {
        while (testyThredy.l < 100000000){
            testyThredy.l += 1;
            System.out.println(l);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print(this.threadName);
        System.out.println(" done!");
    }

    public void start () {
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }

    public void join(){
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

public class AddOne {
    public static void main(String[] args) {
        testyThredy R1 = new testyThredy("first");
        R1.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        testyThredy R2 = new testyThredy("second");
        R2.start();

        R1.join();
        R2.join();
    }
}
