import java.util.LinkedList;

class SharedQueue {
    public static volatile LinkedList<Integer> sharedQueue = new LinkedList<Integer>();
}

class Consumer implements Runnable {
    private Thread t;

    String name;

    public Consumer(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            SharedQueue.sharedQueue.wait();
            System.out.println(SharedQueue.sharedQueue.pop());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void start () {
        if (t == null) {
            t = new Thread (this, name);
            t.start ();
        }
    }
}

class Producer implements Runnable {
    private Thread t;

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SharedQueue.sharedQueue.push(1);
        SharedQueue.sharedQueue.notify();
    }
    public void start () {
        if (t == null) {
            t = new Thread (this);
            t.start ();
        }
    }
}

public class ConsumeProduce {
    public static void main(String[] args) {
        Producer p = new Producer();
        p.start();
        Consumer r1 = new Consumer("r1");
        r1.start();
        Consumer r2 = new Consumer("r2");
        r2.start();
    }
}
