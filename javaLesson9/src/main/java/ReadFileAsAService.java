import org.graalvm.compiler.core.common.util.ReversedList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class ReadFile implements Runnable {
    private Path path;
    public ReadFile(Path path) {
        this.path = path;
    }

    @Override
    public void run() {
        try {
            String read = Files.readAllLines(this.path).get(0);
            System.out.println(read);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

public class ReadFileAsAService {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (String arg: args) {
            executor.execute(new ReadFile(Path.of(arg)));
        }
        executor.shutdown();
        try {
            executor.awaitTermination(100, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
