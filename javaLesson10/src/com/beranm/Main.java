package com.beranm;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class Main {

    public static void main(String[] args) throws IOException {
        DatagramChannel channel = null;
        try {
            channel = DatagramChannel.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String newData = "| \n \n" +
                         "               __\n" +
                         "             <(o )___\n" +
                         "              ( ._> /\n" +
                         "               `---'  ";
        ByteBuffer buf = ByteBuffer.allocate(newData.length() + 1);
        buf.clear();
        buf.put(newData.getBytes());
        buf.flip();

        int bytesSent = channel.send(buf, new InetSocketAddress( "10.0.104.136", 10000));
    }
}
