package com.beranm;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


public class ChatServer {
    public static void main(String[] args) {
        int port = Integer.parseInt("8181");
        List<Socket> sockets = new ArrayList<Socket>();
        ConcurrentHashMap<Integer, Socket> userToSocket = new ConcurrentHashMap<>();

        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("Server is listening on port " + port);

            int i = 0;
            while (true) {
                sockets.add(i, serverSocket.accept());
                System.out.println("New client connected");

                new ServerThread(sockets, i).start();
                i ++;
            }
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
