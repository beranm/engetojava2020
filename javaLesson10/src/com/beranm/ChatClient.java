package com.beranm;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ChatClient {
    public static void main(String[] args) {
        Socket clientSocket = null;
        try {
            clientSocket = new Socket("127.0.0.1", 8181);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                OutputStream outputStream = clientSocket.getOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(new String("abc"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
