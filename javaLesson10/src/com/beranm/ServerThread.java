package com.beranm;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class ServerThread extends Thread {
    private List<Socket> sockets;
    int id;

    public ServerThread(List<Socket> sockets, int id) {
        this.sockets = sockets;
        this.id = id;
    }

    public void run() {
        try {
            InputStream input = sockets.get(this.id).getInputStream();
            ObjectInputStream in = new ObjectInputStream(input);
            String text = (String) in.readObject();
            System.out.println("Somebody said: " + text);
            sockets.get(this.id).close();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
