package com.github.jorgenringen.lambda.stream;

import java.util.*;
import java.util.stream.Collectors;

public class Util {

    public static List<String> mapToUppercase(List<String> input) {
        return input.parallelStream().map(String::toUpperCase).collect(Collectors.toList());
    }

    public static List<String> removeElementsWithMoreThanFourCharacters(List<String> input) {
        return input.parallelStream().filter(i -> i.length() <= 3).collect(Collectors.toList());
    }

    public static Long countStringsWithFiveCharacters(List<String> input) {
        return input.parallelStream().filter(i -> i.length() == 5).count();
    }

    public static List<String> sortStrings(List<String> input) {
        return input.parallelStream().sorted().collect(Collectors.toList());
    }

    public static List<Integer> sortIntegers(List<String> input) {
        return input.parallelStream().map(Integer::parseInt).sorted().collect(Collectors.toList());
    }

    public static List<Integer> sortIntegersDescending(List<String> input) {
        return input.parallelStream().map(Integer::parseInt).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    public static Integer sum(List<Integer> numbers) {
        return numbers.parallelStream().reduce(0, Integer::sum);
    }

    public static Integer multiply(List<Integer> numbers) {
        return numbers.parallelStream().reduce(1, (a, b) -> a * b);
    }

    public static List<String> flattenToSingleCollection(List<List<String>> input) {
        return input.parallelStream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    public static String separateNamesByComma(List<Person> input) {
        return input.parallelStream().map(Person::getName).collect(Collectors.joining(", ", "Names: ", "."));
    }

    public static String findNameOfOldestPerson(List<Person> input) {
        return Objects.requireNonNull(input.parallelStream().max(Comparator.comparing(Person::getAge)).orElse(null)).getName();
    }

    public static List<String> filterPeopleLessThan18YearsOld(List<Person> input) {
        return input.parallelStream().filter(a -> a.getAge() <= 18).map(Person::getName).collect(Collectors.toList());
    }

    public static IntSummaryStatistics getSummaryStatisticsForAge(List<Person> input) {
        return input.parallelStream().collect(Collectors.summarizingInt(Person::getAge));
    }

    public static Map<Boolean, List<Person>> partitionAdults(List<Person> input) {
        return input.parallelStream().collect(Collectors.partitioningBy(a -> a.getAge() >= 18));
    }

    public static Map<String, List<Person>> partitionByNationality(List<Person> input) {
        return input.parallelStream().collect(Collectors.groupingBy(Person::getCountry));
    }
}
