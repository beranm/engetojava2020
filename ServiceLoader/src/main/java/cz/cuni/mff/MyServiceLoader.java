package cz.cuni.mff;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;

public class MyServiceLoader {

    public static <T> Iterable<T> load(Class<T> cl) {
        ArrayList<T> rt = new ArrayList<>();
        System.out.println(cl.getName());
        try {
            Enumeration<URL> urls = MyServiceLoader.class.getClassLoader().getSystemResources("META-INF/services/" + cl.getName());
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                try {
                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    String line = in.readLine();
                    rt.add((T) Class.forName(line).newInstance());
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rt;
    }
}
